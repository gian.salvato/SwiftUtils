//
//  UIColor.swift
//  UIGraphView
//
//  Created by Gianluca Salvato on 08/03/2017.
//  Copyright © 2017 Gianluca Salvato. All rights reserved.
//

import UIKit.UIColor

extension UIColor {

	public static func random(dark: Bool = true, alpha: Double = 0.9) -> UIColor {
		let rand = {
			CGFloat(arc4random_uniform(UInt32.max/2))/CGFloat(UInt32.max) + (!dark ? CGFloat(0.5) : CGFloat(0.0))
		}
		return UIColor(red: rand(), green: rand(), blue: rand(), alpha: CGFloat(alpha))
	}

}

